class Person < ActiveRecord::Base
  include People::Score, People::Round, People::Image

  VK_PERMITTED_FIELDS = [
    :uid, :first_name, :last_name, :sex, :university, :university_name,
    :activities, :interests, :music, :movies, :tv, :books, :games, :about, :quotes
  ].freeze
  VK_REQUEST_FIELDS = (VK_PERMITTED_FIELDS + [:education]).freeze
  DEFAULT_WIDTH = 500
  DEFAULT_SCORE = 400
  MANY_GOOGLIES = 50

  validates :uid, :first_name, :last_name, :sex, presence: true
  validates_uniqueness_of :uid

  def self.vk(mash)
    hash = mash.to_hash.symbolize_keys.slice(*VK_PERMITTED_FIELDS)
    person = where(uid: mash.uid).first
    if person.present?
      person.update_attributes(hash)
      return person
    else
      person = create(hash)
      person.updated_at = nil
      return person
    end
  end

  def in?
    imaged.present?
  end
end
