class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :login_user

  protected

  def login_user
    return if controller_name == 'session'
    deny
  end

  def deny
    redirect_to welcome_path unless lin?
  end

  def lin?
    session[:id].present?
  end

  def person
    @person ||= Person.find(session[:id])
  end
  helper_method :person, :lin?
end
