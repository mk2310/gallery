require 'open-uri'
class ImageSearcher
  IMG_SEARCH_BASE = 'https://www.google.com/searchbyimage?site=search&sa=X&image_url='
  RESULTS         = '#resultStats'
  RESULTS_REGEX   = /About (\d+) result/
  GUESS_CSS       = '#topstuff > div.card-section > div._hUb > a'

  UA              = 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0'

  def initialize(src)
    html = open(IMG_SEARCH_BASE + src, 'User-Agent' => UA)
    @doc = Nokogiri::HTML(html.read)
    @doc.encoding = 'utf-8'
  end

  def matches
    results_s = @doc.css(RESULTS).text
    p results_s
    cnt_s = results_s.try :[], /[\d+\,]+/
    cnt_s = cnt_s.try :tr, ',', ''
    p cnt_s
    return cnt_s.to_i
  end

  def guess
    @doc.css(GUESS_CSS).text
  end
end