class AdminController < ApplicationController
  before_action :admin?

  def chart
    # страничка с графиком
  end

  def load_chart
    # загрузка графика
    limit = (params[:start].present? && params[:start].to_i) || 50
    @qlength = Person.where.not(quotes: nil).order('length(quotes) DESC').limit(limit).pluck('length(quotes)')
    respond_to do |format|
      format.js
    end
  end

  def batch_import
  end

  def do_batch_import
    params[:uids]
    vk = VkontakteApi::Client.new(session[:token])
    info = vk.users.get(user_ids: params[:uids], fields: Person::VK_REQUEST_FIELDS + [:photo_big])
    @people = info.map do |person|
      Person.vk(person)
    end
  end

  private

  def admin?
    redirect_to root_path unless true
  end
end