class AddPhotoBigToPeople < ActiveRecord::Migration
  def change
    add_column :people, :photo_big, :string
  end
end
