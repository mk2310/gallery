module People::Image
  extend ActiveSupport::Concern

  included do
    # before_save :serialize_image_hash
    before_validation :google_image
  end

  def set_image(image_hash)
    self.image_json = image_hash.to_json
    self.score = Person::DEFAULT_SCORE
    self.imaged = Person.image_from_hash(image_hash, Person::DEFAULT_WIDTH, fix: true)
  end

  def unset_image
    self.imaged = nil
    self.image_hash = nil
    self.score = nil
  end

  def image_hash
    @image_hash ||= Oj.load(image_json)
  end

  def serialize_image_hash
    self.image_json = Oj.dump(@image_hash) if @image_hash
  end

  def image(width = nil)
    return imaged unless width
    self.class.image_from_hash(image_hash, width)
  end

  def google_image
    return unless imaged_changed?
    s = ImageSearcher.new(imaged)
    return if (guess = s.guess).blank? && s.matches <= Person::MANY_GOOGLIES
    errors.add :imaged, "#{guess} #{I18n.t 'image.cant_be_set'}"
  end

  module ClassMethods
    SIZES_MAPPING = { 's' => 75, 'm' => 130, 'x' => 604, 'o' => 130, 'p' => 200, 'q' => 320, 'y' => 807, 'z' => 1280, 'w' => 2560 }

    def image_from_hash(hash, width, fix: false)
      attempt_to_fix_hash(hash) if fix
      sizes = hash['sizes']
      sz = sizes.min_by { |x| (x['width'] - width).abs }
      return sz['src'] if sz['width'] != 0
      return bullshit_sizes(sizes)
    end

    def attempt_to_fix_hash(hash)
      sizes = hash['sizes']
      return unless sizes.any? { |s| s['width'] == 0 }
      broken_cnt = 0
      sizes.each do |s|
        width = SIZES_MAPPING[s['type']]
        s['width'] = width || 450
        broken_cnt += 1 if width.nil?
      end
      sizes.last['width'] = 500 if broken_cnt == sizes.size
    end

    def bullshit_sizes(sizes)
      sizes.find { |s| s['type'] =~ /x|y/ }['src'] ||
      sizes.find { |s| s['type'] =~ 'q' }['src'] ||
      sizes.last['src'] ||
      (raise 'Bullshit image')
    end
  end
end