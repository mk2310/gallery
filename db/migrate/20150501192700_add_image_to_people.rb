class AddImageToPeople < ActiveRecord::Migration
  def change
    add_column :people, :imaged, :string
  end
end
