module People::Score
  extend ActiveSupport::Concern

  def beats(other)
    Person.transaction do
      self.update_columns( score: score_change(self.score,  other.score, 1.0))
      other.update_columns(score: score_change(other.score, self.score,  0.0))
    end
  end

  private

  def score_change(a, b, s)
    a + 400.0 * (s - (1.0 / (1.0 + 10.0**((b - a) / 400.0))))
  end
end