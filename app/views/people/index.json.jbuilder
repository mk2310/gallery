json.array!(@people) do |person|
  json.extract! person, :id, :image, :name, :university, :score, :rounds, :quotes
  json.url person_url(person, format: :json)
end
